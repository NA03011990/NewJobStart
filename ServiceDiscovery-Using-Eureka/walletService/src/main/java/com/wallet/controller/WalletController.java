package com.wallet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wallet.walletService.WalletService;

@RestController
@RequestMapping("wallet")
public class WalletController {
	
	@Autowired
	private WalletService walletService;
	
	@GetMapping("{amount}")
	public ResponseEntity<Object> addMoneyToWallet(@PathVariable("amount") double amount){
		return ResponseEntity.status(200).body(walletService.addAmoutToWallet(amount));
	}
	
	@PostMapping("{amount}")
	public ResponseEntity<Object> debitMoney(@PathVariable("amount") double amount){
		return ResponseEntity.status(200).body(walletService.debitAmountFromWallet(amount));
	}

}

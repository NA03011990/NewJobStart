package com.wallet.walletService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.account.walletServiceBeans.Transaction;
import com.account.walletServiceBeans.Wallet;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;

@Service
public class WalletService {
	
	@Autowired
	private RestTemplate restTemplate;
	

	//@CircuitBreaker(name="walletService", fallbackMethod ="buildFallBackWallet")
	@Retry(name="walletService", fallbackMethod ="buildFallBackWallet")
	public Wallet addAmoutToWallet(double amount) {
		Transaction transaction = null;
		transaction = restTemplate.getForObject("http://ACCOUNT-SERVICE/account/"+amount, Transaction.class);
		Wallet wallet = new Wallet(12345, transaction.getAmount()+5000);
		return wallet;
	}
	
	public Wallet buildFallBackWallet(double amount, Throwable t) {
		System.out.println("----inside fallback----------");
		Wallet mywallet = new Wallet(1111,5000);
		return mywallet;
	}
	
	@CircuitBreaker(name="debitFromWalletService", fallbackMethod = "buildFallbackDebitFromWallet")
	public Wallet debitAmountFromWallet(double amount) {
		System.out.println("--- trying to debit from wallet ----");
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setDescription("adding amount "+amount);
		restTemplate.postForObject("http://ACCOUNT-SERVICE/account/"+amount, transaction , Transaction.class);
		Wallet wallet=new Wallet();
		wallet.setWalletNumber(2222);
		wallet.setBalance(20000 - amount);
		return wallet;
	}
	
	public Wallet buildFallbackDebitFromWallet(double amount, Throwable t) {
		System.err.println("----------inside the Fallback Debit......."+amount);
		Wallet wallet = new Wallet(1111,5000);
		return wallet;
	}
	
	
}

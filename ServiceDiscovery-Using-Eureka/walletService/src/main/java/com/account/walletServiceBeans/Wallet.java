package com.account.walletServiceBeans;

public class Wallet {
	
	private int walletNumber;
	private double balance;
	
	public Wallet() {
		super();
	}
	public Wallet(int walletNumber, double balance) {
		super();
		this.walletNumber = walletNumber;
		this.balance = balance;
	}
	public int getWalletNumber() {
		return walletNumber;
	}
	public void setWalletNumber(int walletNumber) {
		this.walletNumber = walletNumber;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "Wallet [walletNumber=" + walletNumber + ", balance=" + balance + "]";
	}
	
	

}

package com.example.authenticationserviceexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableResourceServer // this is a protected resource
@EnableAuthorizationServer // acts as OAuth2 service
public class AuthenticationServiceExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationServiceExampleApplication.class, args);
	}

}

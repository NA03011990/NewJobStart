package com.account.accountService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Service;

import com.account.accountServiceBeans.Transaction;

@Service
public class AccountService {
	
	public Transaction debit(double amount) {
		
		Transaction transaction=new Transaction();
		transaction.setTransactionId(new Double(Math.random() * 123456).intValue());
		transaction.setAmount(amount);
		transaction.setDescription(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy 'T' hh:mm:ss")));
		return transaction;
	}
	
	public Transaction credit(Transaction transaction) {
		transaction.setTransactionId(1124);
		transaction.setAmount(transaction.getAmount()+2000);
		transaction.setDescription("credited :"+transaction.getAmount()+2000);
		return transaction;
		
	}

}
